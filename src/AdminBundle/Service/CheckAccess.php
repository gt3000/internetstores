<?php

namespace AdminBundle\Service;

class CheckAccess implements IsAccessAllowedInterface
{
    /**
     * Allowed header name and value
     *
     * @var array
     */
    private $config = [];

    /**
     * CheckAccess constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function isAccessAllowed(array $headerList = []) : bool
    {
        return isset($headerList[$this->config['access.header']['name']]) &&
               in_array(
                    $this->config['access.header']['value'],
                    $headerList[$this->config['access.header']['name']]
                );
    }


}