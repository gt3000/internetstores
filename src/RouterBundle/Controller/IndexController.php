<?php

namespace RouterBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

class IndexController
{
    /**
     * Default action for Router bundle
     *
     * @return Response
     */
    public function indexAction()
    {
        $response = [
            'status'        => 'success',
            'parameters'    => [
                'controller'    => 'RouterController',
                'action'        => 'index'
            ]
        ];

        return new Response(json_encode($response));
    }
}
