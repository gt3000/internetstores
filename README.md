Router bundle
==============

Router bundle consists event listener which checks if controller was properly defined and can redefine it if needed.

It uses matcher service for match correct controller name from url path.

Controller resolver is used for check is defined controller exist or not.

The same logic added directly to app.dev, so application is able to detect controller from the path immediately as kernel was loaded.

In case of application was not able to find controller, 404 status will be returned. 

Admin bundle
==============

Admin bundle can manage limited access to resources. 

Event listener catches all requests to resources with LimitedAccsessInterface implementation.

Then headers are passed to checkAccess service which returns is resource allowed for current headers or not.

Allowed header name and value are in parameters config file so it can be changed without code modification.

Status 401 returned is access is not allowed


