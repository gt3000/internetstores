<?php

namespace RouterBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RouterBundle
 *
 * Auto routing bundle
 *
 * @package RouterBundle
 */
class RouterBundle extends Bundle
{
}
