<?php

namespace AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AdminBundle
 *
 * Bundle should consist logic with limited access
 *
 * @package AdminBundle
 */
class AdminBundle extends Bundle
{
}
