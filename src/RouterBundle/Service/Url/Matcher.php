<?php

namespace RouterBundle\Service\Url;

use RouterBundle\Service\UrlMatcherInterface;

/**
 * Class Matcher
 *
 * Generate controller name from url path info
 *
 * @package RouterBundle\Service\Url
 */
class Matcher implements UrlMatcherInterface
{

    CONST DEFULT_BUNDLE_NAME        = 'Router';
    CONST DEFULT_CONTROLLER_NAME    = 'Index';
    CONST DEFULT_ACTION_NAME        = 'Index';

    /**
     * @param string $pathinfo
     *
     * @return []
     */
    public function match(string $pathinfo) : array
    {
        $pathData = array_values(
            array_diff(explode('/', $pathinfo), [''])
        );


        return [
            '_controller' => sprintf(
                '%s\Controller\%s::%s',
                $this->getBandleName($pathData[0] ?? self::DEFULT_BUNDLE_NAME),
                $this->getControllerName($pathData[1] ?? self::DEFULT_CONTROLLER_NAME),
                $this->getActionName($pathData[2] ?? self::DEFULT_ACTION_NAME)
            )
        ];
    }

    /**
     * @param string $bundleName
     *
     * @return string
     */
    private function getBandleName(string $bundleName) : string
    {
        return sprintf('%sBundle', ucfirst($bundleName));
    }

    /**
     * @param string $controllerName
     *
     * @return string
     */
    private function getControllerName(string $controllerName) : string
    {
        return sprintf('%sController', ucfirst($controllerName));
    }

    /**
     * @param string $actionName
     *
     * @return string
     */
    private function getActionName(string $actionName) : string
    {
        $actionNamePartList = explode('-', $actionName);
        $actionNamePartList = array_map(function ($part) {
            return ucfirst($part);
        }, $actionNamePartList);

        return sprintf('%sAction', lcfirst(implode('', $actionNamePartList)));
    }

}