<?php

namespace RouterBundle\EventListener;

use RouterBundle\Service\UrlMatcherInterface;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * Class RouterListener
 *
 * Try to auto route if controller has not been foud
 *
 * @package RouterBundle\EventListener
 */
class RouterListener
{

    /** @var  UrlMatcherInterface */
    private $serviceMatcher;

    /** @var  ControllerResolverInterface */
    private $controllerResolver;

    /**
     * RouterListener constructor.
     *
     * @param UrlMatcherInterface           $serviceMatcher
     * @param ControllerResolverInterface   $controllerResolver
     */
    public function __construct(UrlMatcherInterface $serviceMatcher, ControllerResolverInterface $controllerResolver)
    {
        $this->serviceMatcher     = $serviceMatcher;
        $this->controllerResolver = $controllerResolver;
    }


    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $requestException = $event->getRequest()->attributes->get('exception');

        // Try to match url path only if controller was not found
        if ($requestException && $requestException instanceof FlattenException) {

            $event->getRequest()->attributes->add(
                $this->serviceMatcher->match($event->getRequest()->getPathInfo())
            );

            try {

                $event->setController(
                    $this->controllerResolver->getController($event->getRequest())
                );

            } catch (\InvalidArgumentException $e) {
                throw new NotFoundResourceException('Resource has not been found');
            }

        }
    }
}