<?php

namespace RouterBundle\Service;

interface UrlMatcherInterface
{
    /**
     * @param string $pathinfo
     *
     * @return []
     */
    public function match(string $pathinfo) : array;
}