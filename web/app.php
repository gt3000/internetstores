<?php

use Symfony\Component\HttpFoundation\Request;

require __DIR__.'/../vendor/autoload.php';
if (PHP_VERSION_ID < 70000) {
    include_once __DIR__.'/../var/bootstrap.php.cache';
}

$kernel = new AppKernel('prod', false);
if (PHP_VERSION_ID < 70000) {
    $kernel->loadClassCache();
}

$request = Request::createFromGlobals();
$matcher = new \RouterBundle\Service\Url\Matcher();
$controllerResolver = new \Symfony\Component\HttpKernel\Controller\ControllerResolver();

try {

    $request->attributes->add($matcher->match($request->getPathInfo()));

    // Check if controller is callable
    $controller = $controllerResolver->getController($request);

    $response = $kernel->handle($request);

} catch (InvalidArgumentException $e) {

    // Set status 404 if ControllerResolver::getController returns exception
    $response = new \Symfony\Component\HttpFoundation\Response(
        'Not Found',
        \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND
    );

}

$response->send();
$kernel->terminate($request, $response);