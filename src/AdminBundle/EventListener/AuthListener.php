<?php

namespace AdminBundle\EventListener;

use AdminBundle\Controller\LimitedAccsessInterface;
use AdminBundle\Service\IsAccessAllowedInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class AuthListener
 *
 * Allow access to Admin controller only for authenticated requests
 *
 * @package AdminBundle\EventListener
 */
class AuthListener
{
    /** @var  IsAccessAllowedInterface */
    private $serviceCheckAccess;

    /**
     * AuthListener constructor.
     *
     * @param IsAccessAllowedInterface $serviceCheckAccess
     */
    public function __construct(IsAccessAllowedInterface $serviceCheckAccess)
    {
        $this->serviceCheckAccess = $serviceCheckAccess;
    }


    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controllerData = $event->getController();

        if (!is_array($controllerData)) {
            // Can't process this type of controller
            return;
        }

        $objectController = reset($controllerData);

        if ($objectController instanceof LimitedAccsessInterface) {

            $headerList = $event->getRequest()->headers->all();

            if (!$this->serviceCheckAccess->isAccessAllowed($headerList)) {

                $event->setController(function() {
                    return new \Symfony\Component\HttpFoundation\Response(
                        'Access denied',
                        \Symfony\Component\HttpFoundation\Response::HTTP_UNAUTHORIZED
                    );
                });

            }

        }
    }

}