<?php

namespace AdminBundle\Service;

interface IsAccessAllowedInterface
{
    /**
     * @param array $headerList
     *
     * @return bool
     */
    public function isAccessAllowed(array $headerList = []) : bool;
}