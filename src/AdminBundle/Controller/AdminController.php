<?php

namespace AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller implements LimitedAccsessInterface
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $response = [
            'status'        => 'success',
            'parameters'    => [
                'controller'    => 'AdminController',
                'action'        => 'index'
            ]
        ];

        return new Response(json_encode($response));
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function testRouteAction(Request $request)
    {
        $response = [
            'status'        => 'success',
            'parameters'    => [
                'controller'    => 'AdminController',
                'action'        => 'test-route'
            ]
        ];

        return new Response(json_encode($response));
    }
}
